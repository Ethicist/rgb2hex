var x = '0123456789ABCDEF';

function rnd(limit) {
  return Math.floor(Math.random() * limit);
}

function rgb2hex() {
  var input, component, output = '#';
  input = document.getElementById('rgb_input').value;
  if (input) {
    component = input.split(',');
    for (i = 0; i < 3; i++) {
      num = parseInt(component[i]);
      output += x.charAt(num >> 4) + x.charAt(num & 15)
    }
    document.getElementById('hex_result').value = output;
  }
}

function hex2rgb() {
  var output = '';
  input = document.getElementById('hex_input').value;
  if (input) {
    if (input.toString().indexOf("#", 0) === 0) {
      input = input.slice(1);
    }
    input = input.toUpperCase();
    for (i = 0; i < 6; i += 2) {
      output += 16 * x.indexOf(input.charAt(i)) + x.indexOf(input.charAt(i+1));
      if (i < 4) {
        output += ', ';
      }
    }
    document.getElementById('rgb_result').value = output;
  }
}

function update_placeholders() {
  var rgb_example = [rnd(255), rnd(255), rnd(255)].join(", ");
  var hex_example = "#";
  for (i = 0, n = x.length; i < 6; i++) {
    hex_example += x.charAt(rnd(n));
  }
  document.getElementById('rgb_input').placeholder = rgb_example;
  document.getElementById('hex_input').placeholder = hex_example;
}

function update_color(obj_id) {
  var colors;
  if (obj_id == 'rgb_input') {
    colors = document.getElementById(obj_id).value.split(', ');
  } else {
    colors = document.getElementById(obj_id).value.split(/\s/);
  }
  for (i = 0; i < colors.length; i += 1) {
    var css;
    if (obj_id == 'rgb_input') {
      css = "background: rgb(" + colors.join(',') + ");";
    } else {
      if (colors.toString().indexOf("#", 0) === -1) {
    css = "background: #" + colors[i];
      } else {
    css = "background: " + colors[i];
      }
    }
    document.getElementsByClassName(obj_id)[0].setAttribute('style', css);
  }
}

function update_all() {
  rgb2hex();
  update_color('rgb_input');
  hex2rgb();
  update_color('hex_input');
}

window.onload = function() {
  document.getElementsByClassName("btn-scroll")[0].onclick = function() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }
  document.getElementById('rgb_input').onkeyup = function () {
    rgb2hex();
    update_color('rgb_input');
  }
  document.getElementById('hex_input').onkeyup = function () {
    hex2rgb();
    update_color('hex_input');
  }
  document.getElementById('easter').onclick = function () {
    document.getElementsByTagName('html')[0].setAttribute("style", "background: #e3f2e1;");
    document.getElementById('suiseiseki').setAttribute("style", "opacity: 1; left: 0;");
    document.getElementById('rgb_input').value = '235, 135, 135';
    document.getElementById('hex_input').value = '#87C89B';
    update_all();
  }
  document.getElementById('suiseiseki').onclick = function () {
    document.getElementsByTagName('html')[0].setAttribute("style", "background: #f5f5f5;");
    document.getElementById('suiseiseki').setAttribute("style", "left: -240px; opacity: 0;");
    document.getElementById('rgb_input').value = '';
    document.getElementById('hex_input').value = '';
    document.getElementById('rgb_result').value = '';
    document.getElementById('hex_result').value = '';
    update_all();
  }
  update_placeholders();
}

window.onscroll = function() {
  if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
    document.getElementsByClassName("btn-scroll")[0].style.display = "block";
  } else {
    document.getElementsByClassName("btn-scroll")[0].style.display = "none";
  }
};
